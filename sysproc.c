#include "types.h"
#include "x86.h"
#include "defs.h"
#include "date.h"
#include "param.h"
#include "memlayout.h"
#include "mmu.h"
#include "proc.h"


extern int setVariable(char *variable, char *value);
extern int getVariable(char *variable, char *value);
extern int remVariable(char *variable);
extern int wait2(int pid,int* wtime,int* rtime,int* iotime);
extern int set_priority(int priority);

int sys_set_priority(void){
  int priority;
  argint(0, &priority);
  return set_priority(priority);
}

int sys_setVariable(void){
  char *variable;
  char *value;
  if((argstr(0, &variable) < 0) || (argstr(1, &value) < 0)){
    return -2;
  }
  return setVariable(variable, value);
}

int sys_getVariable(void){
  char *variable;
  char *value;
  argstr(0, &variable);
  argstr(1, &value);
  return getVariable(variable, value);
}

int sys_remVariable(void){
  char *variable;
  argstr(0, &variable);
  return remVariable(variable);
}

int sys_yield(void)
{
  yield(); 
  return 0;
}

int
sys_fork(void)
{
  return fork();
}

int
sys_exit(void)
{
  exit();
  return 0;  // not reached
}

int
sys_wait(void)
{
  return wait();
}

int
sys_wait2(void)
{
  int pid; 
  int *wtime; // (𝑤𝑡𝑖𝑚𝑒 = 𝑒𝑡𝑖𝑚𝑒 − 𝑐𝑡𝑖𝑚𝑒 − 𝑖𝑜𝑡𝑖𝑚𝑒 − 𝑟𝑡𝑖𝑚𝑒)
  int *rtime;
  int *iotime;
  argint(0, &pid);
  argptr(1, (char**)&wtime, sizeof(int *));
  argptr(2, (char**)&rtime, sizeof(int *));
  argptr(3, (char**)&iotime, sizeof(int *));
  return wait2(pid,wtime, rtime,iotime);
}

int
sys_kill(void)
{
  int pid;

  if(argint(0, &pid) < 0)
    return -1;
  return kill(pid);
}

int
sys_getpid(void)
{
  return myproc()->pid;
}

int
sys_sbrk(void)
{
  int addr;
  int n;

  if(argint(0, &n) < 0)
    return -1;
  addr = myproc()->sz;
  if(growproc(n) < 0)
    return -1;
  return addr;
}

int
sys_sleep(void)
{
  int n;
  uint ticks0;

  if(argint(0, &n) < 0)
    return -1;
  acquire(&tickslock);
  ticks0 = ticks;
  while(ticks - ticks0 < n){
    if(myproc()->killed){
      release(&tickslock);
      return -1;
    }
    sleep(&ticks, &tickslock);
  }
  release(&tickslock);
  return 0;
}

// return how many clock tick interrupts have occurred
// since start.
int
sys_uptime(void)
{
  uint xticks;

  acquire(&tickslock);
  xticks = ticks;
  release(&tickslock);
  return xticks;
}
