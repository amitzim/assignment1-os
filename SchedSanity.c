#include "types.h"
#include "stat.h"
#include "user.h"

#define NUMPROC 16
#define mediumloopsize 700
#define largeloopsize 5000

// int runtime[NUMPROC] = {0};
// int iotimearr[NUMPROC] = {0};
// int waittime[NUMPROC] = {0};
int pid[NUMPROC] = {0};
int stats[4][3] = {{0}};
int types[4] = {0};

int main(int argc, char *argv[]){
	int i = 0;
	int j = 0;
	for(i = 0 ; i < NUMPROC ; i++){
		if((pid[i] = fork()) == 0){
			int proctype = getpid() % 4;
			#ifdef CFSD
				set_priority((getpid()% 3) + 1);
				// printf(1, "setting priority of pid: %d,with priority: %d\n", getpid() ,(getpid()% 3) + 1);
			#endif
			switch (proctype){
					// printf(1, "setting priority of pid: %d,with priority: %d\n", getpid() ,(getpid()% 3) + 1);
					// printf(1, "setting priority of pid: %d,with priority: %d\n", getpid() ,(getpid()% 3) + 1);
				case 0:
					// set_priority(1);
					for (j = 0 ; j < mediumloopsize ; j++){}
					exit();
					break;
				case 1:
					// set_priority(1);
					for (j = 0 ; j < largeloopsize ; j++){}
					exit();
					break;
				case 2:
					// set_priority(3);
					for (j = 0 ; j < mediumloopsize ; j++){
						printf(1, " ");
					}
					exit();
					break;
				case 3:
					// set_priority(3);
					for (j = 0 ; j < largeloopsize ; j++){
						printf(1, " ");
					}
					exit();
					break;
			}
		}
	}

	for(i = 0 ; i < NUMPROC ; i++){
		int wtime;
		int rtime;
		int iotime;
		
		wait2(pid[i], &wtime, &rtime, &iotime);
		types[pid[i] % 4]++;
		stats[pid[i] % 4][0] += wtime;
		stats[pid[i] % 4][1] += rtime;
		stats[pid[i] % 4][2] += iotime;
	}
	sleep(250);

	for (i = 0 ; i < 4 ; i++){
		printf(1, "\ntype %d\navg wtime: %d\navg rtime: %d\navg iotime: %d\n", i + 1, stats[i][0] / types[i], stats[i][1] / types[i], stats[i][2] / types[i]);
	}
	exit();
}

