#include "types.h"
#include "stat.h"
#include "user.h"

#define NUMPROC 12
#define mediumloopsize 1000
#define largeloopsize 50000

// int runtime[NUMPROC] = {0};
// int iotimearr[NUMPROC] = {0};
// int waittime[NUMPROC] = {0};
int pid[NUMPROC] = {0};
int stats[4][3] = {{0}};
int types[4] = {0};

int main(int argc, char *argv[]){
	int i = 0;
	int j = 0;
	for(i = 0 ; i < NUMPROC ; i++){
		if((pid[i] = fork()) == 0){
			int proctype = getpid() % 4;
			switch (proctype){
				case 0:
					for (j = 0 ; j < mediumloopsize ; j++){}
					exit();
					break;
				case 1:
					for (j = 0 ; j < largeloopsize ; j++){}
					exit();
					break;
				case 2:
					for (j = 0 ; j < mediumloopsize ; j++){
						printf(1, " ");
					}
					exit();
					break;
				case 3:
					for (j = 0 ; j < largeloopsize ; j++){
						printf(1, " ");
					}
					exit();
					break;
			}
		}
	}

	for(i = 0 ; i < NUMPROC ; i++){
		int wtime;
		int rtime;
		int iotime;
		
		wait2(pid[i], &wtime, &rtime, &iotime);
		types[pid[i] % 4]++;
		stats[pid[i] % 4][0] += wtime;
		stats[pid[i] % 4][1] += rtime;
		stats[pid[i] % 4][2] += iotime;
	}
	sleep(500);

	for (i = 0 ; i < 4 ; i++){
		printf(1, "\ntype %d\navg wtime: %d\navg rtime: %d\navg iotime: %d\n", i, stats[i][0]/types[i], stats[i][1]/types[i], stats[i][2]/types[i]);
		//printf(1, "\npid: %d\ntype: %d\nrtime: %d\nwtime: %d\niotime: %d\n", pid[i], pid[i] % 4, runtime[i], waittime[i], iotimearr[i]);
	}
	exit();
}

